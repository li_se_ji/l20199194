﻿#include <graphics.h>
#include <conio.h>
#include <time.h>
#include <stdlib.h>

#define SIZE_ALL_BODY	20
#define SIZE_UNIT_BODY	20
#define WIDTH			480
#define HEIGHT			360		

int x0, y0;
int m_maxx, m_maxy;

typedef enum			//蛇移动的四个方向
{
	left, right, up, down
}DIR;

typedef struct			//结构体化坐标
{
	int x;				//x坐标
	int y;				//y坐标
}COOR;

struct FOOD				//定义食物的信息
{
	int flag;			//食物的状态，1表示没吃，0表示吃了
	COOR crd;			//食物的坐标
}food;

struct SNAKE			//定义蛇的状态信息
{
	int body_num;		//蛇身多少节
	COOR body[SIZE_ALL_BODY];	//蛇身每节的坐标
	DIR dir;					//蛇头方向
}snake;

//游戏结束
void gameover()
{
	settextstyle(20, 20, _T("宋体"));
	outtextxy(40, 40, _T("GAME OVER!"));
	_getch();
	closegraph();
}

//判断是否撞击身体或者边界
void judge_hit()
{
	int i;
	if (snake.body[0].x < x0 || snake.body[0].x > x0 + WIDTH - SIZE_UNIT_BODY ||		//判断撞击边界
		snake.body[0].y < y0 || snake.body[0].y > y0 + HEIGHT - SIZE_UNIT_BODY)
	{
		gameover();
	}
	for (i = snake.body_num - 1; i > 0; i--)							//判断撞击身体
	{
		if (snake.body[0].x == snake.body[i].x && snake.body[0].y == snake.body[i].y)
		{
			gameover();
		}
	}
}



//蛇的移动
void snakemove()
{
	int i;
	for (i = snake.body_num; i > 0; i--)
	{
		snake.body[i].x = snake.body[i - 1].x;
		snake.body[i].y = snake.body[i - 1].y;
	}
	switch (snake.dir)
	{
	case right:
		snake.body[0].x += SIZE_UNIT_BODY; break;
	case left:
		snake.body[0].x -= SIZE_UNIT_BODY; break;
	case up:
		snake.body[0].y -= SIZE_UNIT_BODY; break;
	case down:
		snake.body[0].y += SIZE_UNIT_BODY; break;
	}
	Sleep(100);
}

//键盘控制
void keyboard()
{
	switch (_getch())
	{
	case 'w':
		if (snake.dir != down)
		{
			snake.dir = up;
			break;
		}
	case 'a':
		if (snake.dir != right)
		{
			snake.dir = left;
			break;
		}
	case 's':
		if (snake.dir != up)
		{
			snake.dir = down;
			break;
		}
	case 'd':
		if (snake.dir != left)
		{
			snake.dir = right;
			break;
		}
	}
}

//画蛇
void showsnake()
{
	int i;
	for (i = snake.body_num - 1; i >= 0; i--)
	{
		rectangle(snake.body[i].x, snake.body[i].y, snake.body[i].x + 20, snake.body[i].y + 20);
	}
	setlinecolor(BLACK);
	rectangle(snake.body[snake.body_num].x, snake.body[snake.body_num].y, snake.body[snake.body_num].x + 20, snake.body[snake.body_num].y + 20);
	Sleep(1000);
}


//画食物
void draw_food()
{
	int i = 0;
	i = rand() % 4;
	switch (i)
	{
	case 0:	setlinecolor(RED); break;
	case 1: setlinecolor(GREEN); break;
	case 2: setlinecolor(WHITE); break;
	case 3: setlinecolor(BLUE); break;
	}
	i++;
	rectangle(food.crd.x, food.crd.y, food.crd.x + SIZE_UNIT_BODY, food.crd.y + SIZE_UNIT_BODY);
}

//设置食物信息
void setfood()
{
	srand((unsigned)time(NULL));
	food.crd.x = x0 + rand() % (WIDTH / SIZE_UNIT_BODY) * SIZE_UNIT_BODY;
	food.crd.y = y0 + rand() % (HEIGHT / SIZE_UNIT_BODY) * SIZE_UNIT_BODY;
	food.flag = 1;
}

//蛇吃食物
void eatfood()
{
	if (snake.body[0].x == food.crd.x && snake.body[0].y == food.crd.y)
	{
		snake.body_num++;
		setlinecolor(RED);
		rectangle(food.crd.x, food.crd.y, food.crd.x + SIZE_UNIT_BODY, food.crd.y + SIZE_UNIT_BODY);
		food.flag = 0;
	}
}

//初始化系统
void init_system()
{
	initgraph(700, 600);	//初始化图形界面模式

	m_maxx = getmaxx();
	m_maxy = getmaxy();
	x0 = (m_maxx - WIDTH) / 2;
	y0 = (m_maxy - HEIGHT) / 2;

	snake.body_num = 1;					//初始化蛇的状态
	srand((unsigned)time(NULL));
	snake.body[0].x = x0 + rand() % (WIDTH / SIZE_UNIT_BODY / 2) * SIZE_UNIT_BODY;;
	snake.body[0].y = y0 + rand() % (HEIGHT / SIZE_UNIT_BODY / 2) * SIZE_UNIT_BODY;
	food.flag = 0;

	int dir;
	dir = rand() % 4;
	switch (dir)
	{
	case 0:
		snake.dir = right; break;
	case 1:
		snake.dir = left; break;
	case 2:
		snake.dir = up; break;
	case 3:
		snake.dir = down; break;
	}
	setlinecolor(WHITE);
	rectangle(x0 - 1, y0 - 1, x0 + WIDTH + 1, y0 + HEIGHT + 1);//画游戏框
}



int main()
{
	init_system();
	while (1)
	{
		while (!_kbhit())
		{
			if (!food.flag)
				setfood();	//判断食物是否被吃	
			draw_food();	//画食物
			showsnake();	//画蛇
			snakemove();	//蛇的移动
			judge_hit();	//蛇是否发生撞击
			eatfood();		//吃食物	
		}
		keyboard();
	}
	_getch();
	return 0;
}