﻿#include<stdio.h>
#include<stdlib.h>
#include <time.h>
int menu();
int help();
int error();
int grade1(int b);
int grade2(int b);
int grade3(int b); 
int main()
{
	int n,m=0;
	while (m != 1)
	{
	printf("========口算生成器========\n");
	printf("欢迎使用口算生成器 :\n");
	printf("\n");
	help();
	printf("\n");
	printf("\n");
	menu();
	scanf_s("%d", &n);
	
		switch (n)
		{
		case 1:grade1(n); break;
		case 2:grade2(n); break;
		case 3:grade3(n); break;
		case 4:help(); break;
		case 5:m = 1;printf("程序结束, 欢迎下次使用\n任意键结束……"); break;
		default:error(); break;
		}
	}
}
int help()
{
	printf("帮助信息 ：\n");
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级题目为不超过十位的加减法：\n");
	printf("二年级题目为不超过百位的乘除法：\n");
	printf("三年级题目为不超过百位的加减乘除混合运算题目.\n\n");
	return 1;
}
int menu()
{
	printf("操作列表：\n");
	printf("1)一年级  2)二年级  3)三年级\n");
	printf("4)帮助    5)退出程序\n");
	printf("请输入操作数>");
	return 1;
}
int error()
{
	printf("Error!!!\n");
	printf("错误操作指令，请重新输入\n");
	return 1;
}
int grade1(int b)
{
	int i,n,z,m,k,sum=0;
	char op;
	srand((unsigned)time(NULL));
	printf("<执行操作:)\n");
	printf("现在是一年级题目：\n");
	printf("请输入生成个数>");
	scanf_s("%d", &n);
	for (i = 0; i < n; i++)
	{
		z = rand() % 2;
		if (z == 0) 
			op = '+'; 
			if (z == 1)
				op = '-';
			m = rand() % 10;
			k = rand() % 10;
			if (op == '+')
				sum = m + k;
			else
				sum = m - k;
		printf("%2d%2c%2d=%2d\n",m,op, k,sum);
	}
	printf("执行完了\n");
	return 1;
}
int grade2(int b)
{
	double i, n, z,a,m,sum=0;
	char op;
	srand((unsigned)time(NULL));
	printf("<执行操作:)\n");
	printf("现在是二年级题目：\n");
	printf("请输入生成个数>");
	scanf_s("%lf", &n);
	for (i = 0; i < n; i++)
	{
		z = rand() % 2;
		if (z == 0)
			op = '*';
		if (z == 1)
			op = '/';
		a = rand() % 10;
		if (op=='/'&&a == 0)
			a = rand()%9+1;
		m = rand() % 10;
		if (op == '*')
			sum = m * a;
		else
			sum = m / a;
		printf("%2g%2c%2g=%2g\n",m,op,a,sum);
	}
	printf("执行完了\n");
	return 1;
}
int grade3(int b)
{
	double i, n, z1,z2, a,d,c,sum1=0,sum2=0;
	char op1,op2;
	srand((unsigned)time(NULL));
	printf("<执行操作:)\n");
	printf("现在是三年级题目：\n");
	printf("请输入生成个数>");
	scanf_s("%lf", &n);
	for (i = 0; i < n; i++)
	{
        a = rand() % 100;
		z1 = rand() % 4;
		if (z1 == 0)
			op1= '+';
		if (z1 == 1)
			op1 = '-';
		if (z1 == 2) 
			op1= '*';
		if (z1 == 3) 
			op1 = '/'; 
		d = rand() % 100;
		if (op1 == '/' && d == 0)
			d = rand() % 99 + 1;
		else
			d = rand() % 100;
		if (op1 == '+')
			sum1 = a + d;
		else
			if (op1 == '-')
				sum1 = a - d;
			else
				if (op1 == '*')
					sum1 = a * d;
				else
					sum1 = a / d;
		z2 = rand() % 4;
		if (z2 == 0)
			op2 = '+';
		if (z2== 1)
			op2 = '-';
		if (z2 == 2)
			op2 = '*';
		if (z2 == 3)
			op2 = '/';	
		c = rand() % 100;
		if (op2 == '/' && c == 0)
			c = rand() % 99 + 1;
		else 
			c = rand() % 100;
		if (op2 == '+')
			sum2= sum1 + c;
		else
			if (op2 == '-')
				sum2 = sum1 - c;
			else
				if (op2 == '*')
					sum2 = sum1 * c;
				else
					sum2 = sum1 / c;
		printf("%2g%2c%2g%2c%2g=%2g\n", a,op1,d,op2,c,sum2);
	}
	printf("执行完了\n");
	return 1;
}